<?php
	$potocal = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '');
	$directory = '/air-pollution-monitoring/';
	$base_url = $potocal . '://' . $_SERVER['HTTP_HOST'] . $directory;

	$pm2_5 = (!empty($_GET["pm2_5"])) ? $_GET["pm2_5"] : 0;

	if(($pm2_5 <= 25) && ($pm2_5 >= 0)) {
		$emotion = 'very-good';
		$color = 'rgb(3,169,244, 0.8)';
		$title_text = 'คุณภาพอากาศดีมาก';
		$detail_text = 'คุณภาพอากาศดีมาก เหมาะสำหรับ กิจกรรมกลางแจ้งและการท่องเที่ยว';
	}elseif(($pm2_5 <= 37) && ($pm2_5 > 25)){
		$emotion = 'good';
		$color = 'rgb(76,175,80,0.8)';
		$title_text = 'คุณภาพอากาศดี';
		$detail_text = 'คุณภาพอากาศดี สามารถทำกิจกรรมกลางแจ้งและการท่องเที่ยวได้ตามปกติ';
	}elseif(($pm2_5 <= 50) && ($pm2_5 > 37)){
		$emotion = 'moderate';
		$color = 'rgb(253,216,53,0.6)';
		$title_text = 'ปานกลาง';
		$detail_text = '<b><u>ประชาชนทั่วไป</u></b> : สามารถทำกิจกรรมกลางแจ้งได้ตามปกติ<br><br><b><u>ผู้ที่ต้องดูแลสุขภาพเป็นพิเศษ</u></b> : หากมีอาการเบื้องต้น เช่น ไอ หายใจลำบาก ระคายเคืองตา ควรลดระยะเวลาการทำกิจกรรมกลางแจ้ง';
	}elseif(($pm2_5 <= 90) && ($pm2_5 > 50)){
		$emotion = 'bad';
		$color = 'rgb(255,179,0,0.8)';
		$title_text = 'เริ่มมีผลกระทบต่อสุขภาพ';
		$detail_text = '<b><u>ประชาชนทั่วไป</u></b> : ควรเฝ้าระวังสุขภาพ ถ้ามีอาการเบื้องต้น เช่น ไอ หายใจลำบาก ระคายเคืองตา ควรลดระยะเวลาการทำกิจกรรมกลางแจ้ง หรือ ใช้อุปกรณ์ป้องกันตนเองหากมีความจำเป็น<br><br><b><u>ผู้ที่ต้องดูแลสุขภาพเป็นพิเศษ</u></b> : ควรลดระยะเวลาการทำกิจกรรมกลางแจ้ง หรือ ใช้อุปกรณ์ป้องกันตนเองหากมีความจำเป็น ถ้ามีอาการทางสุขภาพ เช่น ไอ หายใจลำบาก ตาอักเสบ แน่นหน้าอก ปวดศีรษะ หัวใจเต้นไม่เป็นปกติ คลื่นไส้อ่อนเพลีย ควรปรึกษาแพทย์';
	}elseif($pm2_5 > 90){
		$emotion = 'very-bad';
		$color = 'rgb(255,87,34, 0.8)';
		$title_text = 'มีผลกระทบต่อสุขภาพ';
		$detail_text = 'ทุกคนควรวหลีกเลี่ยงกิจกรรมกลางแจ้งทุก หลีกเลี่ยงพื้นที่ที่มีมลพิษทางอากาศสูง หรือใช้อุปกรณ์ป้องกันตนเองหากมีความจำเป็น หากมีอาการทางสุขภาพ ควรปรึกษาแพทย์';
	}

	$station = (!empty($_GET["station"])) ? $_GET["station"] : 1;
	$total_station = (!empty($_GET["total_station"])) ? $_GET["total_station"] : 8;

	$vdo_bg_width = (!empty($_GET["vdo_bg_width"])) ? $_GET["vdo_bg_width"] : '100vw';
	$vdo_bg_height = (!empty($_GET["vdo_bg_height"])) ? $_GET["vdo_bg_height"] : 'auto';
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?=$base_url;?>assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?=$base_url;?>assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Monitoring</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
	<link href="https://fonts.googleapis.com/css?family=Sarabun&display=swap" rel="stylesheet">

	<!-- CSS Files -->
    <link href="<?=$base_url;?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=$base_url;?>assets/css/material-kit.css?v=1.3.0" rel="stylesheet"/>

    <style type="text/css">
    	body, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4 {
    		font-family: "Roboto", "Helvetica", "Arial", "Sarabun", sans-serif;
    	}
    	.title, .card-title, .info-title, .footer-brand, .footer-big h5, .footer-big h4, .media .media-heading {
    		font-family: "Roboto Slab", "Times New Roman", "Sarabun",serif;
    	}
    	body {
    		background-color: #64B5F6;
    	}
    	.header-filter::before {
    		background-color: <?=$color;?>;
    	}
    </style>
</head>

<body class="landing-page">
    <nav class="navbar navbar-danger navbar-transparent navbar-absolute">
    	<div class="container">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
            		<span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
        		</button>
        	</div>

        	<div class="collapse navbar-collapse">
        		<ul class="nav navbar-nav navbar-right">
    				<li>
						<a href="#">
							<i class="material-icons">apps</i> Value
						</a>
					</li>

					<li>
						<a href="#">
							<i class="material-icons">apps</i> Graph
						</a>
					</li>

					<li>
						<a href="#">
							<i class="material-icons">apps</i> Log
						</a>
					</li>
        		</ul>
        	</div>
    	</div>
    </nav>

    <div class="page-header header-filter video-hero jquery-background-video-wrapper demo-video-wrapper" style="overflow: none;">
		<div class="video_container" style="position: absolute;">
	    	<video class="jquery-background-video" style="width: <?=$vdo_bg_width;?>; height: <?=$vdo_bg_height;?>" autoplay muted loop poster="<?=$base_url;?>vdo-bg/thumbnail-bg.jpg">
			    <source src="<?=$base_url;?>vdo-bg/cloud-bg.mp4" type="video/mp4">
			    <source src="<?=$base_url;?>vdo-bg/cloud-bg.webm" type="video/webm">
			    <source src="<?=$base_url;?>vdo-bg/cloud-bg.ogv" type="video/ogg">
			</video>
		</div>

        <div class="container">
        	<div class="row">
        		<div class="col-md-12">
					<h1 class="title">สถานีที่ <?=$station;?></h1>
                    <h4>Last update 2020-01-17 19:07:09 &nbsp;&nbsp;&nbsp;&nbsp; rssi: 23</h4>
				</div>
        	</div>
            <div class="row">
				<div class="col-md-4 text-center">
                    <img src="<?=$base_url;?>images/<?=$emotion;?>.png" alt="" class="img-responsive" style="width: 30vw;">
                    <br>
                    <p style="font-size: 24px;">PM2.5</p>
                    <br>
                    <p style="font-weight: bold; font-size: 72px;">11</p>
                    <br>
                    <p style="font-size: 24px;">24-h avg 12</p>
                    <br>
                    <p style="font-size: 24px;">μg/m<sup>3</sup></p>
				</div>
				<div class="col-md-6">
					<h1 class="title"><?=$title_text;?></h1>
                    <h4><?=$detail_text;?></h4>
                    <br />
                    <div class="col-md-4 text-center">
                    	<p style="font-size: 24px;">PM10</p>
                    	<br>
                    	<p style="font-weight: bold; font-size: 36px;">25</p>
                    	<br>
                    	<p style="font-size: 24px;">μg/m<sup>3</sup></p>
                    </div>
                    <div class="col-md-4 text-center">
                    	<p style="font-size: 24px;">TEMP</p>
                    	<br>
                    	<p style="font-weight: bold; font-size: 36px;">31.3</p>
                    	<br>
                    	<p style="font-size: 24px;">°C</p>
                    </div>
                    <div class="col-md-4 text-center">
                    	<p style="font-size: 24px;">HUMIDITY</p>
                    	<br>
                    	<p style="font-weight: bold; font-size: 36px;">64.3</p>
                    	<br>
                    	<p style="font-size: 24px;">%</p>
                    </div>
				</div>
				<div class="col-md-2">
					<?php for ($i=1; $i <= $total_station ; $i++): ?>
							<p style="font-size: 28px; line-height: 1.5;">สถานีที่ <?=$i;?></p>
					<?php endfor; ?>
				</div>
            </div>
        </div>
    </div>

</body>
	<!--   Core JS Files   -->
	<script src="<?=$base_url;?>assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="<?=$base_url;?>assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--    Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc    -->
	<script src="<?=$base_url;?>assets/js/material-kit.js?v=1.3.0" type="text/javascript"></script>
</html>
